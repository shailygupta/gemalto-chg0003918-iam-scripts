#!/bin/bash

# This script should run periodically to update the users on the machine
timestamp()
{
 date +"%Y-%m-%d %T"
}

echo "[$(timestamp)] Starting the script to update the users on the machine."

unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN AWS_SECURITY_TOKEN

GROUPNAME="group-secops-linuxsuperadmins"
ROLE="role-secops-sshkeyretrieval"
ACCOUNT="128038211799"
DURATION="1800"
NAME="BastionSession"

# KST=access*K*ey, *S*ecretkey, session*T*oken
KST=(`aws sts assume-role --role-arn "arn:aws:iam::$ACCOUNT:role/$ROLE" \
                          --role-session-name "$NAME" \
                          --duration-seconds $DURATION \
                          --query '[Credentials.AccessKeyId,Credentials.SecretAccessKey,Credentials.SessionToken]' \
                          --output text`)

export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:-eu-west-1}
export AWS_ACCESS_KEY_ID=${KST[0]}
export AWS_ACCESS_KEY=${KST[0]}
export AWS_SECRET_ACCESS_KEY=${KST[1]}
export AWS_SECRET_KEY=${KST[1]}
export AWS_SESSION_TOKEN=${KST[2]}
export AWS_SECURITY_TOKEN=${KST[2]}
export AWS_DELEGATION_TOKEN=${KST[2]}

/usr/sbin/groupadd -f sudo

cp /etc/sudoers.d/superadmins /etc/sudoers.d/superadmins.tmp

aws iam get-group --group-name $GROUPNAME --output text |rev|cut -f1|rev | while read UserName; do
  FirstLetter=(`echo $UserName | cut -c1 | tr "[:upper:]" "[:lower:]"`)
  Name=(`echo $UserName | cut -d@ -f1 | cut -d. -f2 | tr "[:upper:]" "[:lower:]"`)
  SaveUserName=$FirstLetter$Name
  SaveUserName=${SaveUserName//"+"/".plus."}
  SaveUserName=${SaveUserName//"="/".equal."}
  SaveUserName=${SaveUserName//","/".comma."}
  if ! id -u "$SaveUserName" >/dev/null 2>&1; then
	SaveUserFileName=$(echo "$SaveUserName" | tr "." " ")
	if [ ${#SaveUserName} -ge 2 ]
	then
		echo "[$(timestamp)] Adding the following user to this machine: $SaveUserName"
		/usr/sbin/useradd -m -G sudo "$SaveUserName" -c "$UserName" -s /bin/bash
		echo "$SaveUserName ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/superadmins.tmp
		mkdir -p /home/$SaveUserName/.ssh
		aws iam list-ssh-public-keys --user-name "$UserName" --query "SSHPublicKeys[?Status =='Active'].[SSHPublicKeyId]" --output text | while read KeyId; do
		aws iam get-ssh-public-key --user-name "$UserName" --ssh-public-key-id "$KeyId" --encoding SSH --query "SSHPublicKey.SSHPublicKeyBody" --output text >> /home/$SaveUserName/.ssh/authorized_keys
		done
		chown -R $SaveUserName:$SaveUserName /home/$SaveUserName
	fi
  fi
done

visudo -cf /tmp/sudoers.d/superadmins.tmp
if [ $? -eq 0 ]; then
  # Replace the sudoers file with the new only if syntax is correct.
  cp /tmp/sudoers.d/superadmins.tmp /etc/sudoers.d/superadmins
else
  echo "Could not modify /etc/sudoers.d/superadmins.tmp file. Please check the log files and /etc/sudoers/superadmins.tmp for syntax errors"
fi


echo "[$(timestamp)] End of script to update the superadmins on the machine."
